<!doctype html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<title>Easy Ajax Image Upload with jQuery and PHP - codingcage.com</title>
    <link rel="stylesheet" href="style.css" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>
<body>
<div class="container">
	<h1><a href="http://www.codingcage.com/2015/12/easy-ajax-image-upload-with-jquery-php.html" target="_blank">Easy Ajax Image Upload with jQuery</a></h1>
	<hr>	

	<div id="preview2"><img src="no-image.jpg" /></div>
	<div id="preview3"><img src="no-image.jpg" /></div>
	<div id="preview4"><img src="no-image.jpg" /></div>
	<?php $numid = rand(1000,1000000); ?>
    <input id="num_id" type="hidden" value="<?php echo $numid ?>" name="num_id" />
	<form id="form1" action="ajaxupload1.php" method="post" enctype="multipart/form-data">
		<div id="preview1"><img src="no-image.jpg" /></div>
		<input id="uploadImage1" type="file" accept="image/*" name="image1" />
		<input type="hidden" name="code1" id="code1" size="10" value="<?php echo $numid ?>">
		<input id="button" type="submit" value="Upload">		
	</form>
	<form id="form2" action="ajaxupload2.php" method="post" enctype="multipart/form-data">
		<input id="uploadImage2" type="file" accept="image/*" name="image2" />
			<input type="hidden" name="code2" id="code2" size="10" value="<?php echo $numid ?>">
		<input id="button" type="submit" value="Upload">		
	</form>
	<form id="form3" action="ajaxupload3.php" method="post" enctype="multipart/form-data">
		<input id="uploadImage3" type="file" accept="image/*" name="image3" />
			<input type="hidden" name="code3" id="code3" size="10" value="<?php echo $numid ?>">
		<input id="button" type="submit" value="Upload">		
	</form>
	<form id="form4" action="ajaxupload4.php" method="post" enctype="multipart/form-data">
		<input id="uploadImage4" type="file" accept="image/*" name="image4" />
			<input type="hidden" name="code4" id="code4" size="10" value="<?php echo $numid ?>">
		<input id="button" type="submit" value="Upload">		
	</form>
    <div id="err"></div>
	<hr>
	<p><a href="http://www.codingcage.com" target="_blank">www.codingcage.com</a></p>
</div>
</body>
</html>