
$(document).ready(function () {
					  userbanned = 0;
// делаем фокус на поле ввода при загрузке страницы
if ($("#chat_text_input").size()>0)
{
    $("#chat_text_input").focus();
}

// функция отправки сообщения
function send_message()
{
    var message_text = $('#chat_text_input').val();
	   var chat_id = $('#chatid').val();
	   	   var mytextcolor = $('#chat_color').val();
    if (message_text!="")
    {

        $.ajax(
        {
            url: 'chat_scripts.php',
            type: 'POST',
            data:
            {
                'action': 'add_message',
				 'chat_id': chat_id,
				 'color': mytextcolor,
                'message_text': message_text
            },
            dataType: 'json',
            success: function (result)
            {

                $('#chat_text_input').val(''); // очищаем поле ввода

                // сразу же подгружаем отправленное сообщение в чат
                get_chat_messages();
            }
         });
     }
 }

 
  function clear_chat()
{
  var message_text = "/clearchat"
	   var chat_id = $('#chatid').val();
	   	   var mytextcolor = $('#chat_color').val();
    if (message_text!="")
    {

        $.ajax(
        {
            url: 'chat_scripts.php',
            type: 'POST',
            data:
            {
                'action': 'add_message',
				 'chat_id': chat_id,
				 'color': mytextcolor,
                'message_text': message_text
            },
            dataType: 'json',
            success: function (result)
            {

                $('#chat_text_input').val(''); // очищаем поле ввода

                // сразу же подгружаем отправленное сообщение в чат
                get_chat_messages();
            }
         });
     }
 }

 // функция подгрузки новых сообщений в чат
 function get_chat_messages()
 {

     // если не выставлена блокировка повторного выполнения данной функции, продолжаем
     if ($('#block').val() == 'no')
     {
         $('#block').val('yes'); // ставим блокировку

         var last_act = $('#last_act').val();
		   var chat_id = $('#chatid').val();
         $.ajax(
         {
             url: 'chat_scripts.php',
             type: 'POST',
             data:
             {
                 'action': 'get_chat_message',
				 'chat_id': chat_id,
                 'last_act': last_act
				 		
             },
             dataType: 'json',
             success: function (result)
             {

			if (result.needtoclear==1) {

				$('#chat_text_field').empty();
			};   
				
				if (result.isbanned!=0) {
					if (result.isoff==1) {
							$('#chat_text_field').empty();
				$('.chatoff').css('display', 'block');
					  $('#block').val('no');
					  $('#chat_text_input').prop('readonly', true);
					    $('#chat_off').val('Включить чат');

				 }
				 else {
				 $('#chat_text_input').prop('readonly', true);
					   $('#chat_text_input').val('Вы забанены до ' + result.isbanned);
					  userbanned = 1;


					$('input[type="submit"]').prop('disabled', true);
					   $('#chat_text_field').append(result.message_code);
                 // обновляем значение последнего сообщения
                 $('#last_act').val(result.last_act);

                 // автопрокрутка текстового поля вниз
                 $('#chat_text_field').scrollTop($('#chat_text_field').scrollTop()+100*$('.chat_post_my, .chat_post_other').size()); 
				   $('#block').val('no');// убираем блокировку
				 }
				 }
				 else 
				if (result.isoff==1) {
					$('#chat_text_field').empty();
				$('.chatoff').css('display', 'block');
					  $('#block').val('no');
					  $('#chat_text_input').prop('readonly', true);
					    $('#chat_off').val('Включить чат');

				 }
				else {
				
					if (result.isbanned==0) {
							if (userbanned==1)
					{
					   $('#chat_text_input').val('');
					 userbanned = 0;


					}
					
				 $('#chat_text_input').prop('readonly', false);
					
					     $('input[type="submit"]').prop('disabled', false);
				 }
					   $('#chat_off').val('Выключить чат');					
						$('.chatoff').css('display', 'none');
                 // добавляем в текстовое поле новые сообщения
                 $('#chat_text_field').append(result.message_code);
                 // обновляем значение последнего сообщения
                 $('#last_act').val(result.last_act);

                 // автопрокрутка текстового поля вниз
                 $('#chat_text_field').scrollTop($('#chat_text_field').scrollTop()+100*$('.chat_post_my, .chat_post_other').size()); 
                 $('#block').val('no');// убираем блокировку
				 }
              }
         });
     }
 }
 

  function get_clear()
 {
  	   var chat_id = $('#chatid').val();
         $.ajax(
         {
             url: 'chat_scripts.php',
             type: 'POST',
             data:
             {
                 'action': 'get_clear',
				 'chat_id': chat_id,				 		
             },
             dataType: 'json',
             success: function (result)
             {
				if (result.getclear==1) {    $('#chat_text_field').empty();};    
          }
         });
     
 }
 
   function chat_off()
 {
  	   var chat_id = $('#chatid').val();
         $.ajax(
         {
             url: 'chat_scripts.php',
             type: 'POST',
             data:
             {
                 'action': 'chat_off',
				 'chat_id': chat_id,				 		
             },
             dataType: 'json',
             success: function (result)
             {

          }
         });
     
 }
 
 
 // отправка сообщений при нажатии клавиши "Enter"
 $('#chat_text_input').keyup(function(event)
 {
     if (event.which == 13)
     {
         send_message();
     }
 });

 // отправка сообщений при нажатии кнопки "Ответить"
 $('#chat_button').click(function()
 {
     send_message();
 });

  $('#clear_chat').click(function()
 {
     clear_chat();
 });
 
   $('#chat_off').click(function()
 {
     chat_off();
 });

 // Действие для кнопки "Выход"
 $('#logout_button').click(function()
 {
     window.location.href = 'index.php?logout';
 });

 // проверяем наличие новых сообщений каждые 2 секунды
 setInterval(function()
 {
     get_chat_messages();
 }, 2000);



 
 // прокрутка текстового поля до последнего сообщения вниз
 $('#chat_text_field').scrollTop($('#chat_text_field').scrollTop()+100*$('.chat_post_my, .chat_post_other').size());

});