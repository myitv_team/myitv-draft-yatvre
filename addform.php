<?php require_once("includes/connection.php"); ?>
<?php include("header.php"); ?>
<?php

$query = 'select id, name, author, playercode, time, chatcode, color1, color2, color3, color4, descr, background, logo, banner, playerbg, studiopass, playerhost, chathost, ownplayer, ownchat, shortname from channels where shortname="'.$_GET['channel'].'"';
$result = $connection->query($query);
while($row = mysqli_fetch_array($result)) 
{
$name=$row['name'];

$author = $row['author'];
$playercode=$row['playercode'];
$mytime = $row['time'];
$channelid= $row['id'];
	$chatcode = $row['chatcode'];
	$color1 = $row['color1'];
	$color2 = $row['color2'];
	$color3 = $row['color3'];
	$color4 = $row['color4'];
	$descr = $row['descr'];
	$background = $row['background'];
	$logo = $row['logo'];
	$banner = $row['banner'];
	$playerbg = $row['playerbg'];
	$studiopass = $row['studiopass'];
	$playerhost = $row['playerhost'];
	$chathost = $row['chathost'];
	$ownplayer = $row['ownplayer'];
	$ownchat = $row['ownchat'];
	$shortname = $row['shortname'];
	


}
?>

<!DOCTYPE html>
<html>
<head>
<title>Submit Form Using AJAX and jQuery</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link href="css/refreshform.css" rel="stylesheet">
<script>
$(document).ready(function(){
$("#submit").click(function(){
var name = $("#name").val();
var commenttext = $("#commenttext").val();
var moduleid = $("#moduleid").val();
var materialid = $("#materialid").val();
// Returns successful data submission message when the entered information is stored in database.
var dataString = 'name1='+ name + '&commenttext1='+ commenttext + '&moduleid1='+ moduleid + '&materialid1='+ materialid;
if(commenttext=='')
{
alert("Заполните поле");
}
else
{
// AJAX Code To Submit Form.
$.ajax({
type: "POST",
url: "ajaxsubmit.php",
data: dataString,
cache: false,
success: function(result){
  $('.results').html(result);

}
});
}
return false;
});
});
</script>
</head>
<body>
<div id="mainform">
<h2>Submit Form Using AJAX and jQuery</h2> <!-- Required div Starts Here -->
<div id="form">
<h3>Fill Your Information !</h3>
<div>
<input id="name" type="text" value = "<?php echo $myuserid; ?>">
<textarea id="commenttext" ></textarea>
<input id="moduleid" type="text" value = "ch">
<input id="materialid" type="text" value = "<?php echo $channelid;?>">
<input id="submit" type="button" value="Submit">
<div class="results"></div>

</div>
</div>
</div>
</body>
</html>