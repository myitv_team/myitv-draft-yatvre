<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- Кодировка UTF-8 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ajax чат</title>
<?php require_once("includes/connection.php"); ?>
<?php include("header.php"); ?>

<!-- Подключаем jQuery и chat_scripts.js-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="chat_scripts.js"></script>
 <script src="jscolor.js"></script>
<style>
#chat_body
{
    width:800px;
    margin:8px auto 3px;
    float:left;
}
#chat_text_field /* Стили для текстового поля */
{
    overflow:auto; /* Отображается полоса прокрутки, когда высота контента > высоты текстового поля */
    border:1px solid #999;
    -webkit-border-radius: 5px 5px 5px 5px;
    -moz-border-radius: 5px 5px 5px 5px;
    border-radius: 5px 5px 5px 5px;
    padding:3px;
    background-color:#fff;
	display:table-cell;
vertical-align:bottom;
}
#chat_text_input /* Стили для поля ввода */
{
    float:left;
    width:600px;
    margin:3px 0;
    font:13px Verdana, Geneva, sans-serif;
    border:1px solid #999;
    -webkit-border-radius: 5px 5px 5px 5px;
    -moz-border-radius: 5px 5px 5px 5px;
    border-radius: 5px 5px 5px 5px;
}
#chat_button
{
    float:left;
    margin:2px 5px 2px 15px;
}
#logout_button
{
    float:left;
    margin:2px 5px;
}
.chat_mess_time
{
    font:14px Verdana, Geneva, sans-serif !important;
	color :#fff !important;
    margin:1px 3px;
}
.chat_nickname
{
    font-weight:bold;
}
.chat_post_my
{
    font:14px Verdana, Geneva, sans-serif !important;
    margin:1px 3px; color:#fff;
}
.chat_post_other
{
    font:14px Verdana, Geneva, sans-serif !important;
    margin:1px 3px; color:#fff;
}
</style>

</head>
<?php $chatname = $_GET['name']; 
?>
<body>

    <div id="chat_body">

        <!--Текстовое поле чата-->
        <div id="chat_text_field" style="height:300px;"></div>

        <!--Номер последнего сообщения-->
        <input id="last_act" name="last_act" type="hidden" value="0" />

        <!--Блокировка повторного выполнения функции get_chat_messages()-->
        <input id="block" name="block" type="hidden" value="no" />
		<input id="chatname" name="chatname" type="text" value="<?php echo $_GET['name'];?>" />

				
				<input type="text" name="color" id="color"  class="input-text" size="7" value="<?php echo $color1; ?>">
				<button class="jscolor
			{valueElement:'color', styleElement:'color'}">
						Выбрать
				</button>
				
				
        <input id="chat_text_input" name="chat_text_input" type="text" />
        <input id="chat_button" name="chat_button" type="button" value="Ответить"/>
        <input id="logout_button" name="logout_button" type="button" value="Выход" />

    </div>
</body>
</html>