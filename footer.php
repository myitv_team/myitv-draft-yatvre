
<div class="clear_both"></div>
</div>
<div class="clear_both"></div>
<div id="footer">
<div class="border"></div> 
<div class="center">
<div class="left">
<h3>Общая информация</h3>
<p><a href="about.html">ЯTV</a></p>

<p><a href="feedback.html">Контакты</a></p>

<p><a href="news.html">Новости</a></p>
<p><a href="press.html">Для СМИ</a></p> 
<p><a href="http://blog.yatv.ru/">Блог разработчиков ЯTV</a></p> 
</div>
<div class="left">
<h3>Платформа</h3>
<p><a href="platform.html">Возможности</a></p> 
<p><a href="http://wiki.yatv.ru/">Справочное руководство</a></p>
<p><a href="stream_promo.html">Организация прямых трансляций</a></p>
<p><a href="http://eagleplatform.com/">Корпоративное решение</a></p>
</div>
<div class="right"> 
<h3>Юридическая информация</h3>
<p><a href="eula.html">Пользовательское соглашение</a></p>
<p><a href="channel_eula.html">Соглашение для владельцев телеканалов</a></p>
<p><a href="rules.html">Правила размещения материалов на сайте</a></p> 
</div>
<div class="right" style="overflow:hidden;">
<h3>ЯTV в сети Интернет</h3>
<p><a href="http://twitter.com/yatv">Twitter</a></p>
<p><a href="http://www.facebook.com/yatvface">Facebook</a></p>
<p><a href="http://vkontakte.ru/club5823466">Вконтакте</a></p>
</div>
</div>
<div class="copyright grey">
<div class="center">

<div class="text">
ЯTV - Я есть телевидение! &copy; 2008-2014 ООО "Далтон Медиа". Создатели данного сайта не имеют к ООО "Далтон Медиа" никакого отношения. Данный сайт - некоммерческий.
</div>
<div class="clear_both"></div>
<div style="padding:10px 10px 0 10px;font-size:10px;">
</div>
</div>
</div> 
</div>

</body>
</html>