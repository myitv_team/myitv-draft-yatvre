<?php require_once("includes/connection.php"); ?>
<?php include("header.php"); ?>
<div id="content">
<div style="margin:10px">
<div class="blackbox"  style="  width: 980px;">
<div class="blackbox_tl"></div>
<div class="blackbox_tc"></div>
<div class="blackbox_tr"></div>
<div class="blackbox_cw">
<div class="blackbox_c" style="">
<div class="blackbox_ci"> 
<div style="height:400px; width:1px; margin-left:-1px; float:left;"></div>
<div style="width:100%; float:left;"><div class="block-content">
<div style="width:650px;height:350px;margin:50px auto;">
<img src="../img/404.icon.png" alt="Страница не найдена" style="float:left;margin-right:30px;">
<div style="float:left;width:500px; padding-top:55px;font-size:14px;">
<h1>Ошибка 404: Страница не найдена.</h1>
<br />
<p><span class="white">Что делать:</span></p>
<br />
<ul>
<li>Если вы вводили адрес вручную в адресной строке браузера, проверьте все ли вы написали правильно.</li>
<li>Если вы перешли по ссылке с другого сайта, попробуйте перейти на первую страницу «ЯTV», вполне вероятно там есть ссылка на нужный вам материал.</li>
<li>А если уж вы встретили неработающую ссылку на страницах «ЯTV», пожалуйста напишите нам об этом.</li>
<li>Также вы можете воспользоваться поиском вверху страницы или картой сайта.</li>
</ul>
</div>
</div>
</div>
<div class="clear_both"></div></div>
<div class="clear_both"></div>
</div><div class="clear_both"></div>
</div></div>
<div class="blackbox_bl"></div>
<div class="blackbox_bc"></div>
<div class="blackbox_br"></div>
</div>

</div>
</div>
<div class="clear_both"></div>
<?php include("footer.php"); ?>