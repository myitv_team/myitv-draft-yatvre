<?php require_once("includes/connection.php"); ?>
<?php include("header_panel.php"); ?>
<?php require('_drawrating.php'); ?>
<?php require('getuserrights.php'); ?>
<script type="text/javascript" language="javascript" src="../js/behavior.js"></script>
<script type="text/javascript" language="javascript" src="../js/rating.js"></script>
<link rel="stylesheet" type="text/css" href="../css/rating.css" />
<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
	<?php
	if(!isset($_COOKIE["session_username"]))  
echo '<script type="text/javascript"> window.location.replace("login.php")</script>';
$query='select id, name, author, playercode, time, chatcode, color1, color2, color3, color4, descr, background, logo, banner, playerbg, studiopass, playerhost, chathost, ownplayer, ownchat, shortname from channels where shortname="'.$_SESSION['channel'].'"';	
	$result = $connection->query($query);
	while($row = mysqli_fetch_array($result)) 
{
$name=$row['name'];

$author = $row['author'];
$playercode=$row['playercode'];
$mytime = $row['time'];
$channelid= $row['id'];
	$chatcode = $row['chatcode'];
	$color1 = $row['color1'];
	$color2 = $row['color2'];
	$color3 = $row['color3'];
	$color4 = $row['color4'];
	$descr = $row['descr'];
	$background = $row['background'];
	$logo = $row['logo'];
	$banner = $row['banner'];
	$playerbg = $row['playerbg'];
	$studiopass = $row['studiopass'];
	$playerhost = $row['playerhost'];
	$chathost = $row['chathost'];
	$ownplayer = $row['ownplayer'];
	$ownchat = $row['ownchat'];
	$shortname = $row['shortname'];
}
?>	
<?php 
	$rights = getuserrights($_COOKIE["session_username"],$channelid);
if ($rights['author'] ||  $rights['poster'] || $rights['moder'] || $rights['broadcaster'] ) {
	?>
<title>Информация о канале | ЯTV - Я есть телевидение!</title>
<body class="theme_default" style="background: transparent url(/img/panelbg.png) !important;">
<div id="content" style="width: 1400px"> 
<!-- LEFT COLUMN>> -->
<div class="left_blocks" style=" width: 310px;" > 
<!-- PROGRAMMS LIST>> -->
<?php 
$curpnst='main';
include 'panelleft.php'; ?>
</div>
<!-- RIGHT COLUMN>> -->
<div class="right_blocks"  style="  width: 1000px;" > 

<div class="blocks"> 
<div class="block_item">
<div style="border-radius: 5px; height:90%; background-color: #444f51;">
<div class="toppanel"><p>Информация о канале</p></div>
<div id="mainbody" style="padding: 10px; ">
<h2>Плеер канала</h2>
<?php if ($ownplayer==0) { echo '<iframe id="player" src="../player.php?channel='.$shortname.'" allowfullscreen scrolling="no" frameborder="0" style="float: left; border: 0px none transparent;height:340px;width:580px;"></iframe>';}
 
?>

<?php if ($ownchat==0) {echo '<iframe src="../newchat.php?id='.$channelid.'" scrolling="no" style="float: left;  width: 400px;height: 340px; border:0;"></iframe>';}
else {if ($chathost=='chatadelic') 
	{echo '<iframe src="http://chatadelic.net/frame.php?chat='.$chatcode.'" style="float: right;width: 370px;  height:330px; border:0; "></iframe>';};
	if ($chathost=='chatovod') 
	{echo '<iframe src="http://'.$chatcode.'.chatovod.ru/" style="float: right;width: 390px;  height:330px; border:0; "></iframe>';}
	 
}; ?>
<br>
<table>
<tr>
<td>
<div class="channelinfoblock"> 
<div class="left">

 <a class="logo" href="#" <?php if ($logo!=='')
{echo 'style="background: #000 url(/img/'.$logo.') no-repeat center center; background-size:100px;"'; } else {echo 'style="background: #000 url(http://yatv-museum.ucoz.com/default_channel_logo.png) no-repeat center center; background-size:120px; "';} ?> ><span class="border_logo"></span></a>
</div>
<div class="right"> 
<h1 class="title darkblue"><?php echo $name; ?></h1>
<div class="rating_block"> 
<div class="fivestar">
<?php echo rating_bar($channelid,'5'); ?>
</div>
<div class="clear_both"></div>
</div>
<div class="info2">
<div class="params">
<div class="label">Владелец:</div>
<div class="value white"><a href="account,userinfo?user=<?php echo $author; ?>"><?php echo $author; ?></a></div>
<div class="clear_both"></div>
</div>



<div class="params">
<div class="label">Создан:</div>

 <div class="value white"><?php echo date('d.m.y', $mytime); ?></div>
<div class="clear_both"></div>
</div> 
</div> 
</div> 
<div class="clear_both"></div> 
<div class="description clear_both">
<div id="descr" class="readmore">

<?php if ($descr!=='')
{echo $descr; } else {echo 'Описание телеканала не заполнено';} ?></div>
</div>

<div class="clear_both"></div>
</div>
</td>
<td>

Записей: <a target=_blank href="../channel,records?shortname=<?php echo $shortname; ?>"><?php

$query = "SELECT records.recordid FROM records where channelid='".$channelid."';";
$result = $connection->query($query);
$numrecs = mysqli_num_rows($result);
echo $numrecs;
?>
</a>
<br>

Анонсов: <a target=_blank href="../channel,programs?shortname=<?php echo $shortname; ?>"><?php

$query = "SELECT programs.programsid FROM programs where  addtime>".time()."  and channelid='".$channelid."';";
$result = $connection->query($query);
$numprogs = mysqli_num_rows($result);
echo $numprogs;
?></a>
<br>
Новостей: <a target=_blank href="../channel,news?shortname=<?php echo $shortname; ?>"><?php

$query = "SELECT news.newsid FROM news where channelid='".$channelid."';";
$result = $connection->query($query);
$numnews = mysqli_num_rows($result);
echo $numnews;
?></a>
<br>
Комментариев: <?php

$query = "SELECT comments.commentid FROM comments where comments.module='ch' AND comments.materialid='".$channelid."';";
$result = $connection->query($query);
echo mysqli_num_rows($result);
?>
<br>
Фанатов: <a target=_blank href="../channel,friends?shortname=<?php echo $shortname; ?>"><?php

$query = "SELECT * FROM likes where likes.module='ch' and likes.materialid='".$channelid."';";
$result = $connection->query($query);
echo mysqli_num_rows($result);
?></a>
</td>
</tr>
</table>


</div> 
</div> 
</div>
</div>
</div>
</div>
<?php } ?>