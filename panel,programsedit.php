<?php require_once("includes/connection.php"); ?>
<?php include("header_panel.php"); ?>
<?php require('getuserrights.php'); ?>
<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" href="css/jquery-ui-timepicker-addon.css">


	<?php
	if(!isset($_COOKIE["session_username"]))  
echo '<script type="text/javascript"> window.location.replace("login.php")</script>';


	$query='select id, name, author, playercode, time, chatcode, color1, color2, color3, color4, descr, background, logo, banner, playerbg, studiopass, playerhost, chathost, ownplayer, ownchat, shortname from channels where shortname="'.$_GET["channel"].'"';
	$result = $connection->query($query);
	while($row = mysqli_fetch_array($result)) 
{
$name=$row['name'];
$author = $row['author'];
$playercode=$row['playercode'];
$channelid= $row['id'];
	$logo = $row['logo'];
	$shortname = $row['shortname'];
}
$query='select title, addtime, descr from programs where programsid='.$_GET["id"];
	$result = $connection->query($query);
	while($row = mysqli_fetch_array($result)) 
{
$title=$row['title'];
$descr = $row['descr'];
$addtime = $row['addtime'];
}
if(isset($_POST["editprog"])){
	if(!empty($_POST['title']) && !empty($_POST['datetime'])) {

	$title=strip_tags($_POST['title']);
	$descr = nl2br(strip_tags($_POST['descr']));
	$res = DateTime::createFromFormat('m/d/Y G:i', $_POST['datetime']);
	$addtime = $res->getTimestamp();
	
	$sql="UPDATE programs 
			SET title = '$title',  
			addtime = '$addtime',  
	descr = '$descr' WHERE programsid=".$_GET['id'];
		$result = $connection->query($sql);
	if($result){
		

		
	  	 $message = "Программа сохранена! <a target=_blank href='programs,view.php?id=".$_GET['id']."'>Перейти к материалу</a>";
	
	} else {
	 $message = "Ошибка!";
	}
	}
	else {
	 $message = "Не заполнены необходимые поля!";
	}
}
?>
  <?php 
	$rights = getuserrights($_COOKIE["session_username"],$_SESSION['channelid']);
if ($rights['author'] ||  $rights['poster']) {
	?>	
 
 
 
	


	
 <title>Редактировать программу | ЯTV - Я есть телевидение!</title>

 <body class="theme_default" style="background: transparent url(/img/panelbg.png) !important;">

<div id="content" > 
 
<!-- LEFT COLUMN>> -->
<div class="left_blocks" style=" width: 310px;" > 

<!-- PROGRAMMS LIST>> -->
<?php 
$curpnst='progsadd';
include 'panelleft.php'; ?>
</div>
<!-- RIGHT COLUMN>> -->
<div class="right_blocks"  style="  width: 610px;" > 

<div class="blocks"> 
<div class="block_item">

<div style="border-radius: 5px; height:90%; background-color: #444f51;">
<div class="toppanel"><p>Редактировать программу </p></div>
<div id="mainbody">
<?php if (!empty($message)) {echo "<div class='info'><p class=\"error\">". $message . "</p></div>";} ?>
<span id="status"></span>
		

</div>


		   
<form name="registerform" id="registerform" action="panel,programsedit.php?id=<?php echo $_GET['id']?>" method="post">
<table>
<tr>
<td style="width: 150px;">
<h3 > Время: </h3>
</td>
<td style="width: 350px;">

<input type="text" name="datetime" id="datetime" value="<?php echo date('m/d/Y H:i', $addtime);?>" />
<script>
$('#datetime').datetimepicker({
		timeOnlyTitle: 'Выберите время',
	timeText: 'Время',
	hourText: 'Часы',
	minuteText: 'Минуты',
	secondText: 'Секунды',
	currentText: 'Сейчас',
	closeText: 'Закрыть',
	closeText: 'Закрыть',
	prevText: '<Пред',
	nextText: 'След>',
	currentText: 'Сегодня',
	monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
	'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
	'Июл','Авг','Сен','Окт','Ноя','Дек'],
	dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
	dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
	dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		stepMinute: 5,
	controlType: 'select',
	oneLine: true,
	timeFormat: 'HH:mm'
});
</script>
		</td>
		</tr>
	<tr>
<td style="width: 150px;">
<h3 > Заголовок: </h3>
</td>
<td style="width: 350px;">
		<input type="text" value="<?php echo $title; ?>" name="title" id="title" style="width:350px; height: 30px"  size="32">
		</td>
		</tr>	
		<tr>
		<td style="width: 150px;">
		<h3>  Краткое описание: </h3>
			</td>
<td style="width: 350px;">
		<textarea name="descr" id="descr" style="width:350px; height: 100px"  size="110" ><?php echo $descr; ?></textarea> 
		</td>
		</tr>
		</table>
					<h4 class="grey">Этот раздел используется для публикации программы передач. Сообщите вашим зрителям о важном мероприятии!</h4>
		<div class="dottedline" style="height:30px;"></div>
		<input type="submit" class="btnsubmit"  name="editprog" id="editprog" value="Добавить программу">
</div>


</form>




	
	
<div class="dottedline" style="height:50px; margin-top: 20px;"></div>
	
	
    <div id="err"></div>


<div id="result_reg"></div>
<div class="clear_both"></div>
</div>
</div>
</div>
<?php } ?>